package com.epam.springg;

import com.epam.springg.otherpack.OtherBeanA;
import com.epam.springg.otherpack.OtherBeanB;
import org.springframework.beans.factory.annotation.Autowired;

public class OutherServicesetter {

    private OtherBeanA otherBeanA;
    private OtherBeanB otherBeanB;

    @Autowired
    public void setOtherBeanA(OtherBeanA otherBeanA) {
        this.otherBeanA = otherBeanA;
    }
    @Autowired
    public void setOtherBeanB(OtherBeanB otherBeanB) {
        this.otherBeanB = otherBeanB;
    }
}
