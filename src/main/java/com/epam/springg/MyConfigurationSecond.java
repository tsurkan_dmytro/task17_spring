package com.epam.springg;

import com.epam.springg.beans3.BeanD;
import com.epam.springg.beans3.BeanF;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.epam.springg.beans2" , "com.epam.springg.beans3"})
public class MyConfigurationSecond {

    @Bean
    BeanD beanDdependency() {
        return new BeanD();
    }

    @Bean
    BeanF beanFdependency() {
        return new BeanF();
    }
}
