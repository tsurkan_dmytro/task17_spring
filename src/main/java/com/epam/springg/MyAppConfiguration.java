package com.epam.springg;

import com.epam.springg.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

@Configuration
@Import(FirstConfig.class)
@PropertySource("my.properties")
public class MyAppConfiguration {
    @Autowired
    private Environment env;


    @Bean("getTeamD")
    public TeamD getTeamD(){
        TeamD teamD = new TeamD();
        teamD.setName(env.getProperty("teamD.name"));
        teamD.setValue(env.getProperty("teamD.value"));

        return teamD;
    }

    @Bean(initMethod = "init", destroyMethod = "clean")
    public TeamB getTeamB(){
        TeamB teamB = new TeamB();
        teamB.setName(env.getProperty("teamB.name"));
        teamB.setValue(env.getProperty("teamB.value"));

        return teamB;
    }

    @Bean("getTeamC")
    public TeamC getTeamC(){
        TeamC teamC = new TeamC();
        teamC.setName(env.getProperty("teamC.name"));
        teamC.setValue(env.getProperty("teamC.value"));

        return teamC;
    }

    @Bean
    public TeamE getTeamE(){  return new TeamE(); }

    @Bean
    @Lazy
    public TeamF getTeamF(){  return new TeamF(); }

    @Bean("TeamABC")
    @DependsOn(value = {"getTeamB","getTeamC"})
    public TeamA getTeamAbc(){
        return new TeamA(getTeamB(), getTeamC());
    }

    @Bean("TeamABD")
    @DependsOn(value = {"getTeamB","getTeamD"})
    public TeamA getTeamAbd(){
        return new TeamA(getTeamB(), getTeamD());
    }

    @Bean("TeamACD")
    @DependsOn(value = {"getTeamC","getTeamD"})
    public TeamA getTeamAcd(){
        return new TeamA(getTeamC(), getTeamD());
    }
}
