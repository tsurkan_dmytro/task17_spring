package com.epam.springg;

import com.epam.springg.model.TeamA;
import com.epam.springg.model.TeamB;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(MyAppConfiguration.class);

     TeamA teamA = ctx.getBean(TeamA.class);
        System.out.println(teamA);

     TeamB teamB = ctx.getBean(TeamB.class);
     teamB.getName();
    }

}
