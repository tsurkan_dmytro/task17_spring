package com.epam.springg.model;

public class TeamF extends TeamEntity{

    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "TeamB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
