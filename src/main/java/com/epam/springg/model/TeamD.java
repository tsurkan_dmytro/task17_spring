package com.epam.springg.model;

import org.springframework.beans.factory.annotation.Value;

public class TeamD extends TeamEntity{

    @Value("${name}")
    private String name;
    @Value("${value}")
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "TeamB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
