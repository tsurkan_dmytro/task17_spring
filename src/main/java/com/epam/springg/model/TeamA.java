package com.epam.springg.model;

import com.epam.springg.DisposableBean;
import com.epam.springg.InitializingBean;
import org.springframework.beans.factory.annotation.Value;

public class TeamA extends TeamEntity implements InitializingBean, DisposableBean  {

    @Value("${name}")
    private String name;
    @Value("${value}")
    private String value;

    private String name1;
    private String name2;

    public TeamA() {
    }

    public TeamA(TeamEntity entity1, TeamEntity entity2) {
        name1 = entity1.getName();
        name2 = entity2.getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "TeamA{" +
                "name='" + name + '\'' +
                "name1='" + name1 + '\'' +
                "name=2'" + name2 + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void disposableBean() {
        System.out.println("Dispose interface");
    }

    @Override
    public void initializingBean() {
        System.out.println("Init interface");
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }
}
