package com.epam.springg.model;

import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;

public class TeamB extends TeamEntity implements Serializable {

    @Value("${name}")
    private String name;
    @Value("${value}")
    private String value;

    public TeamB() {
    }

    public void init() {
        System.out.println("init" + getClass().getName());
    }

    public void clean(){
        System.out.println("destroy" + getClass().getName());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "TeamB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
