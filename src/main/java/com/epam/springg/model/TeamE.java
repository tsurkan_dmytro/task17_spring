package com.epam.springg.model;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class TeamE extends TeamEntity{

    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @PostConstruct
    public void initIt() throws Exception {
        System.out.println("Init method after properties are set : " + name);
    }

    @PreDestroy
    public void cleanUp() throws Exception {
        System.out.println("Spring Container is destroy! Customer clean up");
    }

    @Override
    public String toString() {
        return "TeamB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
