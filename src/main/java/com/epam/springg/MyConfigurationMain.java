package com.epam.springg;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.epam.springg.beans1")
public class MyConfigurationMain {
}
