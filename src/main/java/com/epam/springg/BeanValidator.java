package com.epam.springg;

public interface BeanValidator {
    void validate();
}
