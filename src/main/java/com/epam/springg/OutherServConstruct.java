package com.epam.springg;

import com.epam.springg.otherpack.OtherBeanA;
import com.epam.springg.otherpack.OtherBeanB;
import org.springframework.beans.factory.annotation.Autowired;

public class OutherServConstruct {

    private OtherBeanA otherBeanA;
    private OtherBeanB otherBeanB;

    @Autowired
    public OutherServConstruct(OtherBeanA otherBeanA, OtherBeanB otherBeanB) {
        this.otherBeanA = otherBeanA;
        this.otherBeanB = otherBeanB;
    }
}
